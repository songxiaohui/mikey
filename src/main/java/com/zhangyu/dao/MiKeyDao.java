package com.zhangyu.dao;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface MiKeyDao {

    //查找数据
    List<Map<String , Object> > getMiKeyDataByNo(Integer deviceNo);

    List< Map<String , Object>> getMiKeyData();
}
