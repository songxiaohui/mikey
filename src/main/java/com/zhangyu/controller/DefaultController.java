package com.zhangyu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/view")
public class DefaultController {

    @RequestMapping("/{f}")
    public String defaultView(@PathVariable("f") String f) throws Exception{
        return f;
    }

    @RequestMapping("/{f}/{t}")
    public String defaultView2(@PathVariable("f") String f, @PathVariable("t") String t) throws Exception{

        return f + "/" + t;
    }
}
