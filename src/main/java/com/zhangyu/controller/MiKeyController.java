package com.zhangyu.controller;

import com.zhangyu.service.MiKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
@Scope("prototype")
public class MiKeyController {

    @Autowired
    private MiKeyService miKeyService ;

    @RequestMapping(value = "/mikeys",method = RequestMethod.GET)
    @ResponseBody
    public Map<String , Object> getData(HttpServletRequest request , HttpServletResponse response){

        Map<String , Object > map = new HashMap<>();
        String ids  = request.getParameter("id") == null ? "0" : request.getParameter("id"); //拿到查询条件Id

        int deviceNo = 0;
        try {
            deviceNo = Integer.parseInt(ids);

            if (deviceNo == 0){  // 说明没传id
                Map<String, Object> miKeyData = miKeyService.getMiKeyData(request);
                return miKeyData ;
            }else{
                Map<String, Object> miKeyData = miKeyService.getMiKeyData(request, deviceNo);
                return miKeyData;
            }
        }catch (Exception e){
            map.put("status","error");
            map.put("msg","查询失败");
            return map ;
        }
    }
}
