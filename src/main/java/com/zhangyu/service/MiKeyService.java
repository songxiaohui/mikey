package com.zhangyu.service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface MiKeyService {

    //全部
   Map<String , Object> getMiKeyData(HttpServletRequest request);

   //按 编号
   Map<String , Object> getMiKeyData(HttpServletRequest request , Integer deviceNo );
}
