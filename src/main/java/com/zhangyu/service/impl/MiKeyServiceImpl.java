package com.zhangyu.service.impl;

import com.zhangyu.dao.MiKeyDao;
import com.zhangyu.service.MiKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MiKeyServiceImpl implements MiKeyService {

    @Autowired
    private MiKeyDao miKeyDao ;

    @Override
    public Map<String, Object> getMiKeyData(HttpServletRequest request ) {
        Map<String , Object > map = new HashMap<>();
        List<Map<String, Object>> miKeyData = miKeyDao.getMiKeyData();
        if (miKeyData != null && miKeyData.size() > 0 ){
            map.put("status","success");
            map.put("msg","查询成功");
            map.put("data",miKeyData);
            return map ;
        }else{
            map.put("status","error");
            map.put("msg","查询失败");
            map.put("data",null);
        }
        return map ;
    }

    @Override
    public Map<String, Object> getMiKeyData(HttpServletRequest request, Integer deviceNo) {
        Map<String , Object > map = new HashMap<>();
        List<Map<String, Object>> miKeyDataByNo = miKeyDao.getMiKeyDataByNo(deviceNo);
        if (miKeyDataByNo.size() > 0 ){
        map.put("status","success");
        map.put("msg","查询成功");
        map.put("data",miKeyDataByNo);
        return map ;
        }else{
            map.put("status","error");
            map.put("msg","查询失败");
            map.put("data",null);
            return map;
        }

    }
}
